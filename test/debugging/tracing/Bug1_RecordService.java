package debugging.tracing;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.service.RecordServiceCTL;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;


@ExtendWith(MockitoExtension.class)
class Bug1_RecordService {
    SimpleDateFormat dateFormat;

    Hotel hotel;
    Room room;
    Guest guest;
    Date arrivalDate;
    int sLength;
    int nOccupants;
    CreditCard card;
    RecordServiceCTL serviceCTL;

    @BeforeEach
    void setUp() throws Exception {
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        hotel = new Hotel();
        room = new Room(101, RoomType.SINGLE);
        guest = new Guest("a", "a", 1);
        arrivalDate = dateFormat.parse("11-11-2018");
        sLength = 1;
        nOccupants = 1;
        card = new CreditCard(CreditCardType.VISA, 1, 111);

        serviceCTL = new RecordServiceCTL(hotel);
    }


    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void ReplicateBug1() {
        // arrange
        ServiceType serviceType = ServiceType.ROOM_SERVICE;
        double serviceCost = 15;
        long confNumber = hotel.book(room, guest, arrivalDate, sLength, nOccupants, card);
        hotel.checkin(confNumber);

        // act
        serviceCTL.roomNumberEntered(101);
        serviceCTL.serviceDetailsEntered(serviceType, serviceCost);

    }
}