package debugging.tracing;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.service.RecordServiceCTL;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class Bug2_H4_HotelActiveBookings {
    SimpleDateFormat dateFormat;

    Hotel hotel;
    Room room;
    Guest guest;
    Date arrivalDate;
    int sLength;
    int nOccupants;
    CreditCard card;

    @BeforeEach
    void setUp() throws Exception {
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        hotel = new Hotel();
        room = new Room(101, RoomType.SINGLE);
        guest = new Guest("a", "a", 1);
        arrivalDate = dateFormat.parse("11-11-2018");
        sLength = 1;
        nOccupants = 1;
        card = new CreditCard(CreditCardType.VISA, 1, 111);

    }


    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void H4_CheckActiveBookings() {
        // arrange
        long confNumber = hotel.book(room, guest, arrivalDate, sLength, nOccupants, card);


        // act
        assertTrue(hotel.activeBookingsByRoomId.containsKey(101));
        hotel.checkin(confNumber);
        assertTrue(hotel.activeBookingsByRoomId.containsKey(101));
        hotel.checkout(101);
        // Assert
        assertTrue(hotel.activeBookingsByRoomId.containsKey(101));
    }
}