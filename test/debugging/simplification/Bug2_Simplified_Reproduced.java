package debugging.simplification;

import hotel.checkout.CheckoutCTL;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.service.RecordServiceCTL;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;

@ExtendWith(MockitoExtension.class)
class Bug2_Simplified_Reproduced {
    SimpleDateFormat dateFormat;

    Hotel hotel;
    Room room;
    Guest guest;
    Date arrivalDate;
    int sLength;
    int nOccupants;
    CreditCard card;
    RecordServiceCTL serviceCTL;
    CheckoutCTL checkoutCTL;

    @BeforeEach
    void setUp() throws Exception {
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        hotel = new Hotel();
        room = new Room(101, RoomType.SINGLE);
        guest = new Guest("a", "a", 1);
        arrivalDate = dateFormat.parse("11-11-2018");
        sLength = 1;
        nOccupants = 1;
        card = new CreditCard(CreditCardType.VISA, 1, 111);

        serviceCTL = new RecordServiceCTL(hotel);
        checkoutCTL = new CheckoutCTL(hotel);
    }


    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void ReplicateBug2() {
        // arrange

        long confNumber = hotel.book(room, guest, arrivalDate, sLength, nOccupants, card);
        hotel.checkin(confNumber);
        hotel.addServiceCharge(101, ServiceType.ROOM_SERVICE, 15);
        checkoutCTL.state = CheckoutCTL.State.ROOM;

        // act
        System.out.println("Checking Out\n");
        checkoutCTL.roomIdEntered(101);
        checkoutCTL.chargesAccepted(true);
        checkoutCTL.creditDetailsEntered(CreditCardType.VISA, 1, 111);
        checkoutCTL.completed();

        System.out.println("\nRecording Service");
        serviceCTL.roomNumberEntered(101);
        try {
            serviceCTL.serviceDetailsEntered(ServiceType.ROOM_SERVICE, 10);
        }
        catch(RuntimeException e) { }

    }


}